﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class GameBoard : MonoBehaviour {

	private List<BoardTile> _boardTiles;
	private IEnumerator<BoardTile> _targetSlot;
	private bool _isAnimating;

	public event Action<GameBoard, BoardTile> TilePlaced;
	public event Action<GameBoard, BoardTile, SymbolType> TileRejected;
	public event Action<GameBoard> AllTilesMatched;

	public BoardTile BoardTilePrefab;

	public IEnumerable<BoardTile> Tiles { get {  return _boardTiles;} }

	public BoardTile NextTileToMatch {
		get {
			if( _targetSlot == null )
				return null;

			return _targetSlot.Current;
		}
	}
	
	private SymbolDatabase _symbolDatabase;

	public void Awake()
	{
		_symbolDatabase = GetComponentInParent<SymbolDatabase>();
		_boardTiles = new List<BoardTile>();
	}

	public void Start()
	{
		DestroyAllChildren();
	}

	public void SetBoardDimentions( int width, int height )
	{
		if( _isAnimating )
			throw new InvalidOperationException( "Cannot reset board while animating.");

		DestroyAllChildren();

		_boardTiles.Clear();

		GetComponent<GameBoardLayoutGroup>().CellsPerRow = width;

		for( var i = 0; i < width * height; i++ )
		{
			var boardTile = (BoardTile) Instantiate(BoardTilePrefab);
			boardTile.GameBoard = this;
			boardTile.transform.parent = transform;
			boardTile.transform.localPosition = new Vector3();
			boardTile.transform.localScale = new Vector3( 1, 1, 1) ;
			boardTile.Type = _symbolDatabase.GetRandomSymbol();
			_boardTiles.Add(boardTile);
		}

		transform.localPosition = new Vector3();
	}

	// OfType → Filters the elements of an Enumerable based on a specified type.
	// https://msdn.microsoft.com/es-es/library/bb360913(v=vs.110).aspx

	// ToList → Crea una lista de un Enumerable. (( Para hacer un Enumerable en el caso de abajo ))
	private void DestroyAllChildren()
	{
		foreach( var child in transform.OfType<Transform>().ToList())
			Destroy( child.gameObject );

		/*  In this example, we can see this bit of code compiles no problems. But we'll get a throw exception
			every time that we iterate this code. The above example is right.
		foreach( Transform child in transform )
			Destroy(child);
		*/
	}

	public void ShuffleBoard()
	{
		foreach( var tile in _boardTiles)
			tile.Type = _symbolDatabase.GetRandomSymbol();
	}

	public void ResetBoard()
	{
		foreach( var tile in _boardTiles )
			tile.ResetGame();
	}

	public void BeginGame( IEnumerable<BoardTile> targetTiles, float timePerTile, Action<GameBoard> afterAnimation )
	{
		if( _isAnimating )
			throw new InvalidOperationException( "Cannot start game while animating.");
		var goalTiles = targetTiles.ToList();
		_targetSlot = goalTiles.GetEnumerator();

		if( !_targetSlot.MoveNext() )
		{
			Debug.LogError("No target slots selected for game! ");
			return;
		}

		_isAnimating = true;
		StartCoroutine( BeginGameCo( targetTiles, timePerTile, afterAnimation));
	}

	private IEnumerator BeginGameCo( IEnumerable<BoardTile> targetTiles, float timePerTile, Action<GameBoard> afterAnimation )
	{
		foreach( var tile in targetTiles )
		{
			tile.IsFlipped = true;
			yield return new WaitForSeconds( timePerTile );
			tile.IsFlipped = false;
		}

		_isAnimating = false;
		afterAnimation( this );
	}

	public bool PlaceTile( BoardTile boardTile, SymbolType symbolType )
	{
		if( _targetSlot == null || _targetSlot.Current == null )
			throw new InvalidOperationException(" Game must be started to place tile!");

		if( _targetSlot.Current != boardTile || symbolType != boardTile.Type )
		{
			if( TileRejected != null )
				TileRejected( this, boardTile, boardTile.Type );
			return false;
		}

		boardTile.IsPlaced = true;

		var isFinished = !_targetSlot.MoveNext();

		if( TilePlaced != null )
			TilePlaced( this, boardTile );
		
		if( isFinished && AllTilesMatched != null )
			AllTilesMatched( this );

		return true;
	}
}
