﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System;

public class SymbolDatabase : MonoBehaviour
{
	[SerializeField]
	private Sprite[] normalSprites;

	[SerializeField]
	private Sprite[] droppedSprites;

	private List<SymbolType> allSymbols;
	private List<SymbolType> availableSymbols;

	public IEnumerable<SymbolType> AllSymbols { get { return allSymbols; } }
	public IEnumerable<SymbolType> AvailableSymbols { get { return availableSymbols; } }
	public int AvailableSymbolsCount { get { return availableSymbols.Count; } }

	public void Awake()
	{
		allSymbols = new List<SymbolType>();
		availableSymbols = new List<SymbolType>();

		if( normalSprites.Length != droppedSprites.Length )
			throw new ArgumentException( "Normal Sprites array should be the same lenght as Dropped Sprites array" );

		for( var i = 0; i < normalSprites.Length; i++ )
			allSymbols.Add(new SymbolType( i, normalSprites[i], droppedSprites[i]));
	}

	public void SetAvailableSymbolCount( int availableSymbolCount )
	{
		availableSymbolCount = Mathf.Clamp( availableSymbolCount, 0, allSymbols.Count );

		availableSymbols.Clear();
		availableSymbols.AddRange( allSymbols.OrderBy( v => UnityEngine.Random.Range( 0f, 1f) ).Take( availableSymbolCount ) );
	}

	public SymbolType GetSymbolById( int id )
	{
		return allSymbols[id];
	}

	public SymbolType GetRandomSymbol()
	{
		return availableSymbols[ UnityEngine.Random.Range( 0, availableSymbols.Count) ];
	}
}
