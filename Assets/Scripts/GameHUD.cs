﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour {

	private Animator _animator;
	private GameManager _gameManager;

	public Text PointsText;
	public Text TimeText;
	public Slider PointsSlider;
	public Slider TimeSlider;

	public void Awake()
	{
		_animator = GetComponent<Animator>();
		_gameManager = GetComponentInParent<GameManager>();

		_gameManager.IsPlayingChanged += GameManagerOnIsPlayingChanged;
		_gameManager.PointsChanged += GameMangerOnPointsChanged;
		_gameManager.TimeLeftChanged += GameManagerOnTimeLeftChanged;
		_gameManager.IsGameBoardAnimatingChanged += GameManagerOnIsGameBoardAnimatingChanged;
	}

	private void GameManagerOnIsGameBoardAnimatingChanged( bool isGameBoardAnimating )
	{
		_animator.SetBool("IsDisabled", isGameBoardAnimating);
	}

	private void GameManagerOnTimeLeftChanged( float timeLeft )
	{
		if( !_gameManager.IsPlaying )
			TimeSlider.maxValue = timeLeft;

		TimeText.text = string.Format("{0:F2} seconds", timeLeft );
		TimeSlider.value = timeLeft;
	}

	private void GameMangerOnPointsChanged( int points )
	{
		PointsText.text = string.Format("{0} points", points );
		PointsSlider.value = points;
	}

	private void GameManagerOnIsPlayingChanged( bool isPlayingChanged )
	{
		_animator.SetBool("IsPlaying", isPlayingChanged);
	}
}
