﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class GutterTile : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {

	public DraggedTile DraggedTilePrefab;

	private Image _image;
	private SymbolType _symbolType;
	private bool _isOverDropsite;
	private DraggedTile _currentDraggedTile;
	private Vector2 _targetPositionForTile;

	public SymbolType Type {
		get {  return _symbolType; }
		set {
			_symbolType = value;
			_image.sprite = value.Normal;
		}
	}

	public void Awake()
	{
		_image = GetComponent<Image>();
	}

	public void Update()
	{
		if( _currentDraggedTile == null )
			return;

		if( _isOverDropsite )
		{
			_currentDraggedTile.transform.localPosition =
				Vector2.Lerp(_currentDraggedTile.transform.localPosition, _targetPositionForTile, Time.deltaTime * 10 );
		}
		else
		{
			_currentDraggedTile.transform.localPosition = _targetPositionForTile;
		}
	}

	public void OnDestroy()
	{
		if( _currentDraggedTile == null )
			return;

		Destroy( _currentDraggedTile.gameObject );
	}

	public void OnBeginDrag( PointerEventData eventData )
	{
		_currentDraggedTile = (DraggedTile)Instantiate(DraggedTilePrefab);

		var canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
		_currentDraggedTile.transform.parent = canvasRect;
		_currentDraggedTile.Type = Type;

		Vector2 localPoint;
		RectTransformUtility.ScreenPointToLocalPointInRectangle
			( canvasRect, eventData.position, Camera.main, out localPoint );
		_targetPositionForTile = _currentDraggedTile.transform.localPosition = localPoint;

		_image.enabled = false;
		_isOverDropsite = false; 
	}

	public void OnDrag( PointerEventData eventData )
	{
		if( _isOverDropsite )
			return;

		Vector2 localPoint;
		RectTransformUtility.ScreenPointToLocalPointInRectangle
			( _currentDraggedTile.transform.parent.GetComponent<RectTransform>(), 
			eventData.position, Camera.main, out localPoint);

		_targetPositionForTile = localPoint;
	}

	public void OnEndDrag( PointerEventData eventData )
	{
		_image.enabled = true;
		_currentDraggedTile.Drop( );
		_currentDraggedTile = null;
	}

	public void EnterBoardTile( BoardTile tile )
	{
		_isOverDropsite = true;

		var screenPointOfTile = RectTransformUtility.WorldToScreenPoint( Camera.main, tile.transform.position );
		Vector2 localPoint;
		RectTransformUtility.ScreenPointToLocalPointInRectangle
			( _currentDraggedTile.transform.parent.GetComponent<RectTransform>(), 
			screenPointOfTile, Camera.main, out localPoint );

		_targetPositionForTile = localPoint;
		_currentDraggedTile.IsOverDropsite = true;
	}

	public void ExitBoardTile( BoardTile tile )
	{
		_isOverDropsite = false;
		_currentDraggedTile.IsOverDropsite = false;
	}
}
