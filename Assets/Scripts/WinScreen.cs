﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour {

	public Text PointsText;
	public Text TimeText;

	private GameManager _gameManager;
	private Animator _animator;

	public void Start()
	{
		var rect = GetComponent<RectTransform>();
		rect.anchoredPosition = new Vector2();

		_gameManager = GetComponentInParent<GameManager>();
		_gameManager.GameEnded += GameManagerOnGameEnded;

		gameObject.SetActive( false );

		_animator = GetComponent<Animator>();
	}

	private void GameManagerOnGameEnded( GameEndStatus gameEndStatus )
	{
		if( gameEndStatus == GameEndStatus.Lost )
			return;

		gameObject.SetActive( true );
		PointsText.text = string.Format("With {0} points!", _gameManager.Points );
		TimeText.text = string.Format("{0:F2} seconds left", _gameManager.TimeLeft );

		_animator.SetBool("IsOpen", true );
	}

	public void Close()
	{
		_animator.SetBool("IsOpen", false );
	}

	public void Update()
	{
		if( _animator.GetCurrentAnimatorStateInfo(0).IsName("Disabled") )
			gameObject.SetActive( false );
	}
}
