﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class GameController : MonoBehaviour {

	private GameManager _manager;
	private GameBoard _gameBoard;
	private GameOptions _gameOptions;
	private SymbolDatabase _symbolDatabase;

	public void Start()
	{
		_gameBoard = GetComponentInChildren<GameBoard>();
		_manager = GetComponent<GameManager>();
		_gameOptions = GetComponent<GameOptions>();
		_symbolDatabase = GetComponent<SymbolDatabase>();

		_gameBoard.TileRejected += GameBoardOnTileRejected;
		_gameBoard.TilePlaced += GameBoardOnTilePlaced;
		_gameBoard.AllTilesMatched += GameBoardOnAllTilesMatched;
	}

	public void StartGame()
	{
		_symbolDatabase.SetAvailableSymbolCount( _gameOptions.TypesOfSymbolsCount );
		 
		_manager.TimeLeft = _gameOptions.GameTimeLimit;
		_manager.Points = 0;
		
		_gameBoard.SetBoardDimentions( _gameOptions.TilesPerRow, _gameOptions.TilesPerColumn );
		var tilesToMatch = _gameBoard.Tiles.OrderBy( t => UnityEngine.Random.Range( 0f, 1f) )
			.Take(_gameOptions.TilesToMatch );
		_gameBoard.BeginGame( tilesToMatch, _gameOptions.TimeTilesShown, AfterAnimation);
		_manager.IsGameBoardAnimating = true;
	}

	private void AfterAnimation( GameBoard gameBoard )
	{
		_manager.IsGameBoardAnimating = false;
		_manager.StartGame();
	}

	public void Update()
	{
		if( !_manager.IsPlaying )
			return;

		_manager.TimeLeft -=  Time.deltaTime;

		if( _manager.TimeLeft <= 0 )
			_manager.EndGame( GameEndStatus.Lost );
	}

	private void GameBoardOnAllTilesMatched( GameBoard obj )
	{
		_manager.EndGame(GameEndStatus.Won);
	}

	private void GameBoardOnTilePlaced( GameBoard arg1, BoardTile arg2 )
	{
		_manager.TimeLeft += .5f;
		_manager.Points += 10;
	}

	private void GameBoardOnTileRejected( GameBoard gameBoard, BoardTile boardTile, SymbolType symbolType )
	{
		_manager.Points -= 5; 
	}
}
