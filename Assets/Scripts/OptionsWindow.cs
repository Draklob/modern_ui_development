﻿using UnityEngine;
using System.Collections;

public class OptionsWindow : MonoBehaviour {

	private Animator _animator;
	private GameManager _gameManager;

	public void Awake()
	{
		_animator = GetComponent<Animator>();
		_gameManager = GetComponentInParent<GameManager>();

		_gameManager.IsOptionsWindowOpenChanged += GameManagerOnIsOptionsWindowOpenChanged;
	}
	
	private void GameManagerOnIsOptionsWindowOpenChanged( bool isOptionsWindowOpen )
	{
		_animator.SetBool("IsOpen", isOptionsWindowOpen );
	}
}
	