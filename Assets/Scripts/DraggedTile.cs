﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DraggedTile : MonoBehaviour {

	private Animator _animator;
	private Image _image;
	private SymbolType _symbolType;

	public bool IsOverDropsite {
		get {  return _animator.GetBool("IsOverDropsite"); }
		set { _animator.SetBool("IsOverDropsite", value); }
	}

	public SymbolType Type {
		get { return _symbolType; }
		set {
			_symbolType = value;
			_image.sprite = value.Normal;
		}
	}

	public void Awake()
	{
		_animator = GetComponent<Animator>();
		_image = GetComponent<Image>();
	}

	public void Update()
	{
		if( !_animator.GetCurrentAnimatorStateInfo(0).IsName("Destroy") )
			return;

		Destroy( gameObject );
	}

	public void Drop()
	{
		_animator.SetTrigger("Dropped");
	}
	
}
