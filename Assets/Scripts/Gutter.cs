﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using System.Linq;

public class Gutter : MonoBehaviour {

	public UIBehaviour LeftGutter;
	public UIBehaviour RightGutter;

	public GutterTile GutterTilePrefab;

	private GameManager _manager;
	private SymbolDatabase _symbolDatabase;

	public void Awake()
	{
		_manager = GetComponentInParent<GameManager>();
		_symbolDatabase = GetComponentInParent<SymbolDatabase>();

		_manager.IsPlayingChanged  += ManagerOnIsPLayingChanged;

		DestroyAllChildren();
	}

	private void ManagerOnIsPLayingChanged( bool isPlaying )
	{
		if( isPlaying )
		{
			var symbolPivot = _symbolDatabase.AvailableSymbolsCount /2;
			var index = 0;

			foreach( var symbol in _symbolDatabase.AvailableSymbols )
			{
				var gutter = ++index <= symbolPivot ? LeftGutter : RightGutter;
				var tile = (GutterTile) Instantiate(GutterTilePrefab);
				tile.transform.parent = gutter.transform;
				tile.transform.localScale = new Vector3(1,1,1);
				tile.transform.localPosition = new Vector3();
				tile.Type = symbol;
			}
		}
		else
			DestroyAllChildren();
	}

	private void DestroyAllChildren()
	{
		foreach( var child in LeftGutter.transform.OfType<Transform>().ToList() )
			Destroy( child.gameObject );

		foreach( var child in RightGutter.transform.OfType<Transform>().ToList() )
			Destroy( child.gameObject );
	}
}
