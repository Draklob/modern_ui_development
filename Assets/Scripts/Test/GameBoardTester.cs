﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class GameBoardTester : MonoBehaviour {

	public GameBoard GameBoard;

	private SymbolDatabase _symbolDatabase;
	private GameOptions _gameOptions;
	private bool _isPlaying;

	public void Start()
	{
		_symbolDatabase = GetComponentInParent<SymbolDatabase>( );
		_gameOptions = GetComponentInParent<GameOptions>();

		GameBoard.TilePlaced += GameBoardOnTilePlaced;
		GameBoard.TileRejected += GameBoardOnTileRejected;
		GameBoard.AllTilesMatched += GameBoardOnAllTilesMatched;
	}

	public void StartGame()
	{
		_symbolDatabase.SetAvailableSymbolCount(3);
		GameBoard.SetBoardDimentions( _gameOptions.TilesPerRow, _gameOptions.TilesPerColumn );

		// We need the namespace "System.Linq" to use OrderBy or Take. These come from IEnumerable.
		var tilesToMatch = GameBoard.Tiles.OrderBy( v => UnityEngine.Random.Range( 0f, 1f))
			.Take(_gameOptions.TilesToMatch);
		GameBoard.BeginGame( tilesToMatch, _gameOptions.TimeTilesShown, AfterAnimation);
		Debug.Log("Start Game");
	}

	private void AfterAnimation( GameBoard gameBoard )
	{
		Debug.Log("Game is ready!");
	}

	// IEnumerable.Single → Returns the only element of a sequence that satisfies a specified condition, 
	//						and throws an exception if more than one such element exists.
	// https://msdn.microsoft.com/en-us/library/bb535118(v=vs.110).aspx

	// IEnumerable.FirstOrDefault → Return the first element of the sequence that satisfies a condition
	//								or a default value if no such element is found.
	// https://msdn.microsoft.com/en-us/library/bb549039(v=vs.110).aspx

	public void PlaceInvalidTile()
	{
		var boardTile = GameBoard.Tiles.FirstOrDefault( t => t != GameBoard.NextTileToMatch && !t.IsPlaced );
		GameBoard.PlaceTile(boardTile, boardTile.Type );
	}

	public void PlaceValidTile()
	{
		GameBoard.PlaceTile(GameBoard.NextTileToMatch, GameBoard.NextTileToMatch.Type );
	}

	private void GameBoardOnAllTilesMatched( GameBoard gameBoard )
	{
		// Remember that "using System" is needed to use the next line.
		// throw new NotImplementedException( );

		Debug.Log("All tiles matched!");
		_isPlaying = false;
	}

	// string.Format → It allows us to convert the value of objects to string based
	//				   and inserts them into another string.

	private void GameBoardOnTileRejected( GameBoard gameBoard, BoardTile boardTile, SymbolType arg3 )
	{
		Debug.Log( string.Format( "Tile {0} rejected on slot {1}", arg3.Id, boardTile.Type.Id ), boardTile);
	}

	private void GameBoardOnTilePlaced( GameBoard gameBoard, BoardTile boardTile )
	{
		Debug.Log( string.Format( "Tile {0} placed", boardTile.Type.Id ));
	}
}
