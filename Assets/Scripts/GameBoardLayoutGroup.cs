﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// UIBehaviour necesita el namespace "UnityEngine.EventSystems" para su uso y heredar sus funciones.

	[ExecuteInEditMode]
public class GameBoardLayoutGroup : UIBehaviour, ILayoutGroup
{
	public Vector2 CellSize;
	public int CellsPerRow;

	private RectTransform _rect;
	private RectTransform Rect {
		get {  return _rect = (_rect == null ) ? _rect = GetComponent<RectTransform>() : _rect; }
	}

	public void SetLayoutHorizontal()
	{
		if( CellsPerRow <= 0 )
			return;

		var columnCount = Mathf.Ceil( Rect.childCount / ( float ) CellsPerRow -1 );

		var currentRow = 0;
		var currentColumn = 0;
		var startX = -( ( float ) CellsPerRow ) * CellSize.x / 2f + ( CellSize.x / 2 );
		var startY = -columnCount * CellSize.y /2f;

		for( var i = 0; i < Rect.childCount; i++ )
		{
			var child = Rect.GetChild(i) as RectTransform;
			if( child == null )
				continue;

			child.sizeDelta = CellSize;
			child.anchoredPosition = new Vector2( startX + currentColumn * CellSize.x,
												startY + currentRow * CellSize.y );

			currentColumn++;

			if( currentColumn % CellsPerRow == 0 )
			{
				currentColumn = 0;
				currentRow++;
			}
		}

		Rect.sizeDelta = new Vector2( CellsPerRow * CellSize.x, ( columnCount + 1 ) * CellSize.y );
	}

	public void SetLayoutVertical()
	{
		
	}

	// This function is called when the script is loaded or a value is changed in the inspector( Called in the editor only).
	// Use this function to validate the data. This can be used to ensure that when -
	//		you modify date in an editor that the data stays within a certain range.
	protected override void OnValidate()
	{
		if( CellsPerRow < 1)
			CellsPerRow =1;

		if( CellSize.x < 1 )
			CellSize.x = 1;

		if( CellSize.y < 1 )
			CellSize.y = 1;

		SetDirty();
	}

	protected override void OnRectTransformDimensionsChange()
	{
		SetDirty();
	}

	protected override void OnBeforeTransformParentChanged()
	{
		SetDirty();
	}

	protected virtual void OnTransformChildrenChanged()
	{
		SetDirty();
	}

	private void SetDirty()
	{
		
		// En el caso de que se esté reconstruyendo el layout, sale de la función.
		if ( CanvasUpdateRegistry.IsRebuildingLayout() )
			return;
		
		// LayourRebuilder - Mark the given RectTransform as needing it's layout to be recalculated during the next layout pass.
		// Mark the given RectTransform as needing it's layout to be recalculated during the next layout pass.
		// Here, we're saying to Unity we need to run this code again (the layout)
		LayoutRebuilder.MarkLayoutForRebuild( Rect );

	}
}
