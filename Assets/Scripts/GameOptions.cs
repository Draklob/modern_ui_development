﻿using UnityEngine;
using System.Collections;

public class GameOptions : MonoBehaviour {

	public int TilesPerRow { get; set; }
	public int TilesPerColumn { get; set; }
	public int TilesToMatch { get; set; }
	public int TypesOfSymbolsCount { get; set; }

	public float TimeTilesShown { get; set; }
	public float GameTimeLimit {  get; set; }

	public void SetTilesPerRow( float value )
	{
		TilesPerRow = (int) value;
	}

	public void SetTilesPerColumn( float value )
	{
		TilesPerColumn = (int) value;
	}

	public void SetTilesToMatch( float value )
	{
		TilesToMatch = (int) value;
	}

	public void SetTypesOfSymbolsCount( float value )
	{
		TypesOfSymbolsCount = (int) value;
	}
}
